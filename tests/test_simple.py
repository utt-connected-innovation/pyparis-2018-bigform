def test_simple():
    form = Form(schema='/schema/simple.json',
        data={'A':3, 'B':2})

    form.A = 3
    form.B = 2
    assert form.C == 3 + 2

    form.A = 4
    assert form.C == 4 + 2

    form.B = 6
    assert form.C == 4 + 6    