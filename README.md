# pyparis-2018-bigform

To set up the environment :
```
python3 -m venv env
source env/bin/activate
pip install -r pip_requirements.txt

apt install -y entr silversearcher-ag
```

To start the jupyter notebook :
```
jupyter-lab
```

To start the static server :
```
python -m http.server
```


* [transcrypt](https://www.transcrypt.org/) site
* [json-editor](https://github.com/json-editor/json-editor) github project
* json-editor [live demo](https://json-editor.github.io/json-editor/)


