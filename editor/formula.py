'''
// Get a reference to a node within the editor
var name = editor.getEditor('root.name');

// `getEditor` will return null if the path is invalid
if(name) {
  name.setValue("John Smith");

  console.log(name.getValue());
}
'''

print('Hello from formula.py !')

print(document)

def init(x):
    return x*x

class MyEditor:
    def bind(self, jsoneditor):
        print('Binding to jsoneditor ...')
        self.jsoneditor = jsoneditor

    def value(self, path):
        # Get a reference to a node within the editor
        #name = self.jsoneditor.getEditor('root.name');
        node = self.jsoneditor.getEditor(path)

        # `getEditor` will return null if the path is invalid
        #if(name) {
        #name.setValue("John Smith");

        print(node)
        value = (node.getValue())
        return value


editor = MyEditor()
